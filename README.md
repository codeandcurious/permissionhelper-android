![tandem-innovation](tandem-innovation.png)

# android-kotlin-permissionhelper
PermissionHelper is a simple library made in kotlin to request user permission in the android API >= 23.

Latest version: 1.0.1 (22/10/2018) minApi: 21

Gradle setup
------------
**Project gradle:**
```
repositories {
      maven { url "https://dl.bintray.com/tandem/com.tandeminnovation.android" }
    }
```
**Module gradle:**
```
dependencies {
    implementation 'com.tandeminnovation.android:permissionhelper:1.0.1'
}
```
Usage
------------
**Note:** Do not request permissions in onResume, it will enter in a endless loop

**Create a constant to be used as request code**

`private const val PERMISSION_REQUEST_CODE = 1987`


**Request your permissions**

```
private fun requestPermissions() {
val permissionBuilder: PermissionHelper.PermissionBuilder = PermissionHelper.with(yourActivity/Fragment)
.build(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
.onPermissionsDenied(object : OnDenyAction() {
        override fun call(requestCode: Int, shouldShowRationale: Boolean) {
            //TODO: take an action when denied
            }
       })
.onPermissionsGranted(object : OnGrantAction() {
        override fun call(requestCode: Int) {
                //TODO: take an action when granted
        }
   }).request(PERMISSION_REQUEST_CODE)
}
```

**Handle the request overriding onRequestPermissionsResult in any activity or fragment**

```
override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
permissionBuilder!!.onRequestPermissionsResult(requestCode, permissions, grantResults)
super.onRequestPermissionsResult(requestCode, permissions, grantResults)
   }
```

License
-------
    Copyright 2018 Tandem Innovation. All rights reserved.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
