/*
 * Copyright 2018 Tandem Innovation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tandeminnovation.permissionhelper.helper

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.tandeminnovation.permissionhelper.interfaces.OnPermissionsDenied
import com.tandeminnovation.permissionhelper.interfaces.OnPermissionsGranted

object PermissionHelper {

    private lateinit var requestPermissionsLauncher: ActivityResultLauncher<Array<out String>>
    private var onPermissionsGranted: OnPermissionsGranted? = null
    private var onPermissionsDenied: OnPermissionsDenied? = null

    fun register(appCompatActivity: AppCompatActivity) {
        requestPermissionsLauncher = appCompatActivity.registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { isGrantedMap: MutableMap<String, Boolean>? ->

            val grantedPermissions = mutableListOf<String>()
            val deniedPermissions = mutableListOf<String>()
            isGrantedMap?.entries?.forEach {
                when {
                    it.value -> grantedPermissions.add(it.key)
                    else -> deniedPermissions.add(it.key)
                }
            }

            if (grantedPermissions.isNotEmpty()) {
                onPermissionsGranted?.onPermissionsGranted(grantedPermissions.toList())
            }
            if (deniedPermissions.isNotEmpty()) {
                onPermissionsDenied?.onPermissionsDenied(deniedPermissions.toList())
            }
        }
    }

    /**
     * Will request the user for the permission
     * Initializes the listener for permission results.
     * When available, the result of the request will be delivered in [onPermissionsGranted] and [onPermissionsDenied]
     * @param permissions the list of permissions from [Manifest.permission] to be requested
     * @param onPermissionsGranted callback of granted permissions if they exist
     * @param onPermissionsDenied callback of denied permissions if they exist
     */
    fun requestPermissions(permissions: Array<String>, onPermissionsGranted: OnPermissionsGranted, onPermissionsDenied: OnPermissionsDenied? = null) {
        this.onPermissionsGranted = onPermissionsGranted
        this.onPermissionsDenied = onPermissionsDenied
        requestPermissionsLauncher.launch(permissions)
    }

    /**
     * Checks it the app has the permission
     * @param context Context
     * @param permission the permission from [Manifest.permission] to be requested
     * @return true if has permissions or false if it doesn't have
     */
    fun hasPermission(context: Context, permission: String): Boolean {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED
    }

    /**
     * Checks it the app has the permissions for multiple permissions
     * @param context Context
     * @param permissions the permissions from [Manifest.permission] to be requested
     * @return list in the same order with the result of permission result
     */
    fun hasPermissions(context: Context, permissions: MutableList<String>): List<Boolean> {
        return permissions.map { hasPermission(context, it) }
    }

}