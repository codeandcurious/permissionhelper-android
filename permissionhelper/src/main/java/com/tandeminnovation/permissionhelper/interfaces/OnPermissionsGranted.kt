package com.tandeminnovation.permissionhelper.interfaces

fun interface OnPermissionsGranted {

    fun onPermissionsGranted(permissions: List<String>)
}