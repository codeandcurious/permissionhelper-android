package com.tandeminnovation.permissionhelper.interfaces

fun interface OnPermissionsDenied {

    fun onPermissionsDenied(permissions: List<String>)
}